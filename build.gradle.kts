import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jooq.meta.jaxb.Database
import org.jooq.meta.jaxb.Property

plugins {
    idea
    id("org.springframework.boot") version "2.2.2.RELEASE"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
    kotlin("kapt") version "1.3.61"
    application
}

group = "lezhin.test.pyj246"
version = "1.0.0"
java.sourceCompatibility = JavaVersion.VERSION_11

val developmentOnly by configurations.creating
configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
}

extra["springCloudVersion"] = "Hoxton.SR1"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    // Spring
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.2.7.RELEASE")

    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    // flyway
    implementation("org.flywaydb:flyway-core")

    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Swagger
    implementation("io.springfox:springfox-swagger2:2.9.2")
    implementation("io.springfox:springfox-swagger-ui:2.9.2")

    // Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    // Postgresql
    runtimeOnly("com.h2database:h2")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
}

sourceSets {
    main {
        java {
            srcDirs("build/generated/source/jooq/main/java")
        }
    }
}

buildscript {
    dependencies {
        classpath("org.jooq:jooq:3.12.3")
        classpath("org.jooq:jooq-meta-extensions:3.12.3")
        classpath("org.jooq:jooq-codegen:3.12.3")
    }
}

open class JooqGeneratorTask : DefaultTask() {
    private val targetDirectory = "build/generated/source/jooq/main/java"
    private val targetPackageName = "lezhin.test.pyj246.persistence"

    private val outputDirectoryName by lazy {
        "./$targetDirectory/${targetPackageName.replace(".", "/")}"
    }

    @get:OutputDirectory
    val outputDirectory: File by lazy {
        File(outputDirectoryName).let { if (it.isAbsolute) it else project.file(outputDirectoryName) }
    }

    @TaskAction
    fun generate() {
        println("Jooq Code generate using Flyway Migration SQL Files")
        val configuration = org.jooq.meta.jaxb.Configuration().withGenerator(
                org.jooq.meta.jaxb.Generator()
                        .withDatabase(
                                Database()
                                        .withName("org.jooq.meta.extensions.ddl.DDLDatabase")
                                        .withProperties(
                                                Property().withKey("scripts").withValue("src/main/resources/db/migration/"),
                                                Property().withKey("sort").withValue("flyway"),
                                                Property().withKey("unqulifiedSchema").withValue("none"),
                                                Property().withKey("defaultNameCase").withValue("lower")
                                        )
                                        .withInputSchema("PUBLIC")
                                        .withOutputSchemaToDefault(true)
                        )
                        .withTarget(
                                org.jooq.meta.jaxb.Target()
                                        .withDirectory(targetDirectory)
                                        .withPackageName(targetPackageName)
                        )
        )
        org.jooq.codegen.GenerationTool.generate(configuration)
        println("Jooq Code Generated!.")
    }
}

tasks.register<JooqGeneratorTask>("jooq")
tasks.getByName("compileKotlin").dependsOn(":jooq")

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
