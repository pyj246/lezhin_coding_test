# 레진 코딩테스트

#### 개발 환경

Kotlin, Spring boot, Jooq, Flyway, H2

#### 폴더구조

1. config : 설정 파일이 있는 패키지
2. controller : 컨트롤러 파일이 있는 패키지
3. exception : 예외처리 관련 파일이 있는 패키지
4. helper : 로직과 관련없는 도움도구가 있는 패키지
5. model : API 모델이 있는 패키지
6. security : 인증처리와 관련있는 파일이 있는 패키지
7. service : 비지니스 로직을 처리하는 파일이 있는 패키지

#### 인증처리
1. 사용자 인증은 Spring Security 의 OAuth2 Authorization Server 를 이용하여 구현하며, 인증 방식은 JWT 입니다.
2. 인증요청 토큰 정보
    * clientId : lezhin-test-pyj246
    * password : password
    * Basic bGV6aGluLXRlc3QtcHlqMjQ2OnNlY3JldA==
3. 테스트 계정 정
    * user01@test.com / password
    * user02@test.com / password
    * user03@test.com / password
    * user04@test.com / password
    * user05@test.com / password

#### Swagger
http://localhost:8080/swagger-ui.html
    