package lezhin.test.pyj246.security

import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.RSAKey
import net.minidev.json.JSONObject
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerSecurityConfiguration
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPublicKey

@Configuration
class KeyConfiguration {
    @Bean
    fun keyPair(): KeyPair {
        val gen = KeyPairGenerator.getInstance("RSA")
        gen.initialize(2048)
        return gen.generateKeyPair()
    }
}

@FrameworkEndpoint
class JwkSetEndpoint(private val keyPair: KeyPair) {
    @GetMapping("/.well-known/jwks.json")
    @ResponseBody
    fun jwks(): JSONObject {
        val key = RSAKey.Builder(keyPair.public as RSAPublicKey).build()
        return JWKSet(key).toJSONObject()
    }
}

@Configuration
class JwkSetEndpointConfiguration : AuthorizationServerSecurityConfiguration() {
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        super.configure(http)
        http
            .requestMatchers().mvcMatchers("/.well-known/jwks.json")
            .and()
            .authorizeRequests().anyRequest().permitAll()
    }
}
