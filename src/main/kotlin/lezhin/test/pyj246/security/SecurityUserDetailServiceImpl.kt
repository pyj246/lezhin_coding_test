package lezhin.test.pyj246.security

import lezhin.test.pyj246.persistence.Tables.PASSWORD
import lezhin.test.pyj246.persistence.Tables.USER
import org.jooq.DSLContext
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component


@Component("securityUserDetailService")
class SecurityUserDetailServiceImpl(
    private val dslContext: DSLContext
): UserDetailsService {

    override fun loadUserByUsername(email: String?): UserDetails {
        val record = dslContext
            .select()
            .from(USER)
            .join(PASSWORD)
                .on(PASSWORD.USER_KEY.eq(USER.USER_KEY))
            .where(USER.EMAIL.eq(email))
            .fetchOptional()
            .orElseGet { throw UsernameNotFoundException("사용자(email: ${email}) 정보 조회 실패") }

        val userRecord = record.into(USER)
        val passwordRecord = record.into(PASSWORD)

        return User.builder()
            .username(userRecord.email)
            .password(passwordRecord.password)
            .authorities(SimpleGrantedAuthority("ROLE_USR"))
            .build()
    }
}