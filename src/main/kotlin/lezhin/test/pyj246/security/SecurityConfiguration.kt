package lezhin.test.pyj246.security

import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.crypto.password.PasswordEncoder

@EnableWebSecurity
class WebSecurityConfiguration(
    private val securityUserDetailService: UserDetailsService
) : WebSecurityConfigurerAdapter(true) {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.requestMatchers().antMatchers("/v1/**")
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/v1/posts/*", "/v1/followers/*", "/v1/newfeed/all").permitAll()
            .anyRequest().authenticated()
            .and()
            .cors().disable()
            .anonymous()
            .and()
            .oauth2ResourceServer().jwt()
    }

    @Throws(Exception::class)
    override fun configure(authManagerBuilder: AuthenticationManagerBuilder) {
        val daoAuthenticationProvider = DaoAuthenticationProvider()
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder())
        daoAuthenticationProvider.setUserDetailsService(securityUserDetailService)

        authManagerBuilder.authenticationProvider(daoAuthenticationProvider)
    }

    @Throws(Exception::class)
    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder()
    }
}