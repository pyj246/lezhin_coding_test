package lezhin.test.pyj246.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import java.security.KeyPair

@Import(AuthorizationServerEndpointsConfiguration::class)
@Configuration
class AuthorizationServerConfiguration(
    private val authenticationManager: AuthenticationManager,
    private val securityUserDetailService: UserDetailsService,
    private val keyPair: KeyPair
) : AuthorizationServerConfigurerAdapter() {

    companion object {
        private const val MAX_ACCESS_TOKEN_VALID_SECOND = 60 * 60
    }

    @Throws(Exception::class)
    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.inMemory()
            .withClient("lezhin-test-pyj246")
            .secret("{noop}secret")
            .authorizedGrantTypes("password", "refresh_token", "authorization_code")
            .scopes("none")
            .accessTokenValiditySeconds(MAX_ACCESS_TOKEN_VALID_SECOND)
    }

    @Throws(Exception::class)
    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        endpoints.authenticationManager(authenticationManager)
            .userDetailsService(securityUserDetailService)
            .tokenStore(tokenStore())
            .accessTokenConverter(accessTokenConverter())
    }

    @Bean
    fun tokenStore(): TokenStore {
        return JwtTokenStore(accessTokenConverter())
    }

    @Bean
    fun accessTokenConverter(): JwtAccessTokenConverter {
        val accessTokenConverter = DefaultAccessTokenConverter()
        accessTokenConverter.setUserTokenConverter(SubjectAttributeUserTokenConverter())

        val jwtAccessTokenConverter = JwtAccessTokenConverter()
        jwtAccessTokenConverter.setKeyPair(keyPair)
        jwtAccessTokenConverter.accessTokenConverter = accessTokenConverter
        return jwtAccessTokenConverter
    }
}

class SubjectAttributeUserTokenConverter : DefaultUserAuthenticationConverter() {
    override fun convertUserAuthentication(authentication: Authentication): Map<String, Any?> {
        val authenticationMap = mutableMapOf<String, Any?>()
        authenticationMap.putAll(super.convertUserAuthentication(authentication))
        authenticationMap["sub"] = authentication.name
        return authenticationMap
    }
}
