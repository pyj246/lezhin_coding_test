package lezhin.test.pyj246.exception

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

enum class ErrorCode {
    INVALID_ARGUMENTS,
    NO_DATA,
    NO_PERMISSION,
    ERROR
}

class NotFoundException(override val message: String, override val code: ErrorCode = ErrorCode.NO_DATA) :
    BaseException(message, code)

class NoPermissionException(override val message: String, override val code: ErrorCode = ErrorCode.NO_PERMISSION) :
    BaseException(message, code)

class InternalServerException(override val message: String, override val code: ErrorCode) :
    BaseException(message, code)

abstract class BaseException(
    override val message: String,
    open val code: ErrorCode = ErrorCode.ERROR
) : RuntimeException("[$code] $message")

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ErrorResponse(
    @JsonProperty(value = "code")
    val code: ErrorCode,

    @JsonProperty(value = "items")
    val itemList: List<ErrorItem>? = null
)

data class ErrorItem(
    @JsonProperty(value = "item")
    val item: String,

    @JsonProperty(value = "error")
    val error: String
)
