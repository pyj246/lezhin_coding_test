package lezhin.test.pyj246.exception

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

/**
 * 예외처리 핸들러.
 */
@RestControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    /**
     * NotFound(500) 처리.
     */
    @ExceptionHandler(NotFoundException::class)
    fun notFoundHandler(
        exception: NotFoundException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> {
        logger.error(exception.message, exception)
        return ResponseEntity(ErrorResponse(exception.code), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    /**
     * NoPermission(500) 처리.
     */
    @ExceptionHandler(NoPermissionException::class)
    fun noPermissionHandler(
        exception: NotFoundException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> {
        logger.error(exception.message, exception)
        return ResponseEntity(ErrorResponse(exception.code), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    /**
     * Internal Server(500) 처리.
     */
    @ExceptionHandler(InternalServerException::class)
    fun internalServerErrorHandler(
        exception: InternalServerException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> {
        logger.error(exception.message, exception)
        return ResponseEntity(ErrorResponse(exception.code), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    override fun handleMethodArgumentNotValid(
        exception: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        logger.error(exception.message, exception)
        val errorItemList = exception.bindingResult.fieldErrors.map {
            ErrorItem(it.field, it.codes?.last() ?: "")
        }
        return ResponseEntity(ErrorResponse(ErrorCode.INVALID_ARGUMENTS, errorItemList), HttpStatus.BAD_REQUEST)
    }

    override fun handleExceptionInternal(
        exception: java.lang.Exception,
        body: Any?,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        // handleExceptionInternal 인 경우 상세 오류가 표시 안 되는 문제점이 있어 override 처리.
        logger.error("handleExceptionInternal", exception)
        return super.handleExceptionInternal(exception, body, headers, status, request)
    }
}
