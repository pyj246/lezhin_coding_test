package lezhin.test.pyj246.helper

import lezhin.test.pyj246.model.page.PaginatedList
import lezhin.test.pyj246.model.page.Page
import org.jooq.Record
import org.jooq.SelectForUpdateStep
import org.jooq.SelectLimitStep

/**
 * 전체 개수 조회.
 * @return 전체 개수.
 */
fun <R : Record?> SelectLimitStep<R>.total() = this.configuration().dsl().fetchCount(this)

/**
 * 리미트와 오프셋 추가.
 * @param page 페이징 정보.
 * @return 리미트와 오프셋이 추가된 단계
 */
fun <R : Record?> SelectLimitStep<R>.limitOffset(page: Page): SelectForUpdateStep<R> =
    this.limit(page.limit).offset((page.no - 1) * page.limit)

/**
 * 페이징 처리.
 * @param page 페이징 정보.
 * @param function 결과 처리 함수.
 * @return 페이징 결과.
 */
fun <R : Record?, T> SelectLimitStep<R>.pagination(
    page: Page,
    function: (record: R) -> T
): PaginatedList<T> {
    val total = this.total()
    page.total = total
    return PaginatedList(
        page = page,
        list = when (total > 0) {
            true -> this.limitOffset(page).map { function(it) }
            false -> emptyList()
        }
    )
}