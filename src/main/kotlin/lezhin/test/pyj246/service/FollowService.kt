package lezhin.test.pyj246.service

import lezhin.test.pyj246.model.user.User
import lezhin.test.pyj246.persistence.tables.records.UserRecord

/**
 * 팔로우 서비스 인터페이스
 */
interface FollowService {

    /**
     * 팔로우 생성.
     * @param user      팔로우하는 사용자 레코드.
     * @param following 팔로우 대상 사용자 레코드.
     */
    fun createFollowing(user: UserRecord, following: UserRecord)

    /**
     * 팔로우 삭제(언팔로우, unfollow).
     * @param user      팔로우하는 사용자 레코드.
     * @param following 팔로우 대상 사용자 레코드.
     */
    fun deleteFollowing(user: UserRecord, following: UserRecord)

    /**
     * 사용자가 팔로우하고 있는 사용자 목록 조회.
     * @param user      사용자 레코드.
     */
    fun getFollowingList(user: UserRecord): List<User>

    /**
     * 사용자를 팔로우하고 있는 사용자 목록 조회.
     * @param user      사용자 레코드.
     */
    fun getFollowerList(user: UserRecord): List<User>

}