package lezhin.test.pyj246.service.impl

import lezhin.test.pyj246.model.user.User
import lezhin.test.pyj246.persistence.Tables.*
import lezhin.test.pyj246.persistence.tables.records.FollowRecord
import lezhin.test.pyj246.persistence.tables.records.UserRecord
import lezhin.test.pyj246.service.FollowService
import org.jooq.DSLContext
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime

/**
 * 팔로우 서비스 구현클래스.
 */
@Service
class FollowServiceImpl(
    private val dslContext: DSLContext
) : FollowService {

    @Transactional
    override fun createFollowing(user: UserRecord, following: UserRecord) {
        val followRecord = getFollowRecord(user.userKey, following.userKey)
        if (followRecord == null) {
            val newRecord = dslContext.newRecord(FOLLOW)
            newRecord.userKey = user.userKey
            newRecord.following = following.userKey
            newRecord.store()
            newRecord.refresh()

            createFollowHistory(newRecord)

        } else if (followRecord.deleted == true) {
            followRecord.deleted = false
            followRecord.updateAt = OffsetDateTime.now()
            followRecord.store()

            createFollowHistory(followRecord)
        }
    }

    @Transactional
    override fun deleteFollowing(user: UserRecord, following: UserRecord) {
        val followRecord = getFollowRecord(user.userKey, following.userKey)
        if (followRecord != null && followRecord.deleted == false) {
            followRecord.deleted = true
            followRecord.updateAt = OffsetDateTime.now()
            followRecord.store()

            createFollowHistory(followRecord)
        }
    }

    override fun getFollowingList(user: UserRecord): List<User> =
        dslContext
            .select()
            .from(FOLLOW)
            .join(USER).on(USER.USER_KEY.eq(FOLLOW.FOLLOWING))
            .where(FOLLOW.USER_KEY.eq(user.userKey))
            .and(FOLLOW.DELETED.isFalse)
            .map {
                val userRecord = it.into(USER)
                User(
                    userId = userRecord.userId,
                    alias = userRecord.alias
                )
            }

    override fun getFollowerList(user: UserRecord): List<User> =
        dslContext
            .selectDistinct(USER.USER_ID, USER.ALIAS)
            .from(FOLLOW)
            .join(USER).on(USER.USER_KEY.eq(FOLLOW.USER_KEY))
            .where(FOLLOW.FOLLOWING.eq(user.userKey))
            .and(FOLLOW.DELETED.isFalse)
            .map {
                val userRecord = it.into(USER)
                User(
                    userId = userRecord.userId,
                    alias = userRecord.alias
                )
            }

    /**
     * 팔로우 레코드 조회.
     * @param userKey       사용자 식별자.
     * @param followerKey   팔로우하는 사용자 식별자.
     * @return 팔로우 레코드.
     */
    private fun getFollowRecord(userKey: Long, followerKey: Long): FollowRecord? =
        dslContext.selectFrom(FOLLOW).where(
            FOLLOW.USER_KEY.eq(userKey), FOLLOW.FOLLOWING.eq(followerKey)
        ).fetchOptional().orElseGet { null }

    /**
     * 팔로우 히스토리 레코드 생성.
     * @param followRecord  팔로우 레코드.
     */
    private fun createFollowHistory(followRecord: FollowRecord) {
        val historyRecord = dslContext.newRecord(FOLLOW_HIS)
        historyRecord.followKey = followRecord.followKey
        historyRecord.userKey = followRecord.userKey
        historyRecord.following = followRecord.following
        historyRecord.deleted = followRecord.deleted
        historyRecord.createAt = followRecord.updateAt
        historyRecord.store()
    }

}