package lezhin.test.pyj246.service.impl

import lezhin.test.pyj246.exception.NotFoundException
import lezhin.test.pyj246.persistence.Tables.USER
import lezhin.test.pyj246.persistence.tables.records.UserRecord
import lezhin.test.pyj246.service.UserService
import org.jooq.DSLContext
import org.springframework.stereotype.Service

/**
 * 사용자 서비스 구현클래스.
 */
@Service
class UserServiceImpl(
    private val dslContext: DSLContext
) : UserService {

    override fun getUserRecordByUserId(userId: String): UserRecord =
        dslContext.selectFrom(USER).where(USER.USER_ID.eq(userId))
            .fetchOptional().orElseThrow { NotFoundException("사용자(id: ${userId}) 정보 조회 실패") }

    override fun getUserRecordByEmail(email: String): UserRecord =
        dslContext.selectFrom(USER).where(USER.EMAIL.eq(email))
            .fetchOptional().orElseThrow { NotFoundException("사용자(email: ${email}) 정보 조회 실패") }
}