package lezhin.test.pyj246.service.impl

import lezhin.test.pyj246.exception.NoPermissionException
import lezhin.test.pyj246.exception.NotFoundException
import lezhin.test.pyj246.helper.pagination
import lezhin.test.pyj246.model.page.PaginatedList
import lezhin.test.pyj246.model.page.Page
import lezhin.test.pyj246.model.post.Post
import lezhin.test.pyj246.model.post.PostRequest
import lezhin.test.pyj246.persistence.Tables.*
import lezhin.test.pyj246.persistence.tables.records.PostRecord
import lezhin.test.pyj246.persistence.tables.records.UserRecord
import lezhin.test.pyj246.service.PostService
import org.jooq.DSLContext
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime

/**
 * 포스트 서비스 구현클래스.
 */
@Service
class PostServiceImpl(
    private val dslContext: DSLContext
) : PostService {

    @Transactional
    override fun createPost(post: PostRequest, user: UserRecord) {
        val newRecord = dslContext.newRecord(POST)
        newRecord.contents = post.contents
        newRecord.userKey = user.userKey
        newRecord.store()
        newRecord.refresh()

        createPostHistory(newRecord)
    }

    @Transactional
    override fun updatePost(postKey: Long, post: PostRequest, user: UserRecord) {
        val postRecord = getPostRecord(postKey)
        if (postRecord.userKey != user.userKey) {
            throw NoPermissionException("포스트(key: ${postKey})를 수정할 수 없는 사용자(id: ${user.userId})")
        }

        postRecord.contents = post.contents
        postRecord.updateAt = OffsetDateTime.now()
        postRecord.store()

        createPostHistory(postRecord)
    }

    @Transactional
    override fun deletePost(postKey: Long, user: UserRecord) {
        val postRecord = getPostRecord(postKey)
        if (postRecord.userKey != user.userKey) {
            throw NoPermissionException("포스트(key: ${postKey})를 삭제할 수 없는 사용자(id: ${user.userId})")
        }

        postRecord.deleted = true
        postRecord.updateAt = OffsetDateTime.now()
        postRecord.store()

        createPostHistory(postRecord)
    }

    override fun getPost(postKey: Long): Post =
        dslContext
            .select(POST.asterisk(), USER.ALIAS, USER.USER_ID)
            .from(POST)
            .join(USER).on(USER.USER_KEY.eq(POST.USER_KEY))
            .where(POST.POST_KEY.eq(postKey))
            .and(POST.DELETED.isFalse)
            .fetchOptional()
            .map {
                val postRecord = it.into(POST)
                val userRecord = it.into(USER)
                convertToPost(postRecord, userRecord.alias, userRecord.userId)
            }
            .orElseThrow { throw NotFoundException("포스트(key: ${postKey}) 정보 조회 실패") }

    override fun getPostList(user: UserRecord, page: Page): PaginatedList<Post> =
        dslContext
            .select(POST.asterisk(), USER.ALIAS, USER.USER_ID)
            .from(POST)
            .join(USER).on(USER.USER_KEY.eq(POST.USER_KEY))
            .where(POST.USER_KEY.eq(user.userKey))
            .and(POST.DELETED.isFalse)
            .orderBy(POST.POST_KEY.desc())
            .pagination(page) {
                val postRecord = it.into(POST)
                val userRecord = it.into(USER)
                convertToPost(postRecord, userRecord.alias, userRecord.userId)
            }

    override fun getNewsfeed(user: UserRecord, page: Page): PaginatedList<Post> =
        dslContext
            .select(POST.asterisk(), USER.ALIAS, USER.USER_ID)
            .from(POST)
            .join(USER).on(USER.USER_KEY.eq(POST.USER_KEY))
            .join(FOLLOW).on(FOLLOW.FOLLOWING.eq(POST.USER_KEY))
            .where(FOLLOW.USER_KEY.eq(user.userKey))
            .and(POST.DELETED.isFalse)
            .orderBy(POST.POST_KEY.desc())
            .pagination(page) {
                val postRecord = it.into(POST)
                val userRecord = it.into(USER)
                convertToPost(postRecord, userRecord.alias, userRecord.userId)
            }

    override fun getNewsfeed(page: Page): PaginatedList<Post> =
        dslContext
            .select(POST.asterisk(), USER.ALIAS, USER.USER_ID)
            .from(POST)
            .join(USER).on(USER.USER_KEY.eq(POST.USER_KEY))
            .and(POST.DELETED.isFalse)
            .orderBy(POST.POST_KEY.desc())
            .pagination(page) {
                val postRecord = it.into(POST)
                val userRecord = it.into(USER)
                convertToPost(postRecord, userRecord.alias, userRecord.userId)
            }

    /**
     * 포스트 레코드 조회.
     * @param postKey   포스트 식별자.
     * @return 포스트 레코드.
     */
    private fun getPostRecord(postKey: Long) = dslContext
        .selectFrom(POST)
        .where(POST.POST_KEY.eq(postKey))
        .and(POST.DELETED.isFalse)
        .fetchOptional()
        .orElseThrow { throw NotFoundException("포스트(key: ${postKey}) 정보 조회 실패") }

    /**
     * 포스트 레코드를 포스트 정보로 변환.
     * @param postRecord    포스트 레코드.
     * @param writerAlias   작성자 별칭.
     * @param writerId      작성자 아이디.
     * @return 포스트 정보.
     */
    private fun convertToPost(
        postRecord: PostRecord,
        writerAlias: String,
        writerId: String
    ): Post =
        Post(
            postKey = postRecord.postKey,
            contents = postRecord.contents,
            writerAlias = writerAlias,
            writerId = writerId,
            createAt = postRecord.createAt,
            updateAt = postRecord.updateAt
        )

    /**
     * 포스트 히스토리 레코드 생성.
     * @param postRecord    포스트 레코드.
     */
    private fun createPostHistory(postRecord: PostRecord) {
        val historyRecord = dslContext.newRecord(POST_HIS)
        historyRecord.postKey = postRecord.postKey
        historyRecord.userKey = postRecord.userKey
        historyRecord.contents = postRecord.contents
        historyRecord.deleted = postRecord.deleted
        historyRecord.createAt = postRecord.updateAt
        historyRecord.store()

    }
}