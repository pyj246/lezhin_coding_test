package lezhin.test.pyj246.service

import lezhin.test.pyj246.model.page.PaginatedList
import lezhin.test.pyj246.model.page.Page
import lezhin.test.pyj246.model.post.Post
import lezhin.test.pyj246.model.post.PostRequest
import lezhin.test.pyj246.persistence.tables.records.UserRecord

/**
 * 포스트 서비스 인터페이스.
 */
interface PostService {

    /**
     * 포스트 생성.
     * @param post      포스트 정보.
     * @param user      사용자 레코드.
     */
    fun createPost(post: PostRequest, user: UserRecord)

    /**
     * 포스트 수정.
     * @param postKey   포스트 식별자.
     * @param post      포스트 정보.
     * @param user      사용자 레코드.
     */
    fun updatePost(postKey: Long, post: PostRequest, user: UserRecord)

    /**
     * 포스트 삭제.
     * @param postKey   포스트 식별자.
     * @param user      사용자 레코드.
     */
    fun deletePost(postKey: Long, user: UserRecord)

    /**
     * 포스트 조회.
     * @param postKey   포스트 식별자.
     * @return 포스트 정보.
     */
    fun getPost(postKey: Long): Post

    /**
     * 포스트 목록 조회.
     * @param user      사용자 레코드.
     * @param page      페이지 정보.
     * @return 포스트 정보 목록.
     */
    fun getPostList(user: UserRecord, page: Page): PaginatedList<Post>

    /**
     * 뉴스피드 조회.
     * @param user      사용자 레코드.
     * @param page      페이지 정보.
     * @return 포스트 정보 목록.
     */
    fun getNewsfeed(user: UserRecord, page: Page): PaginatedList<Post>

    /**
     * 뉴스피드 조회.
     * @param page      페이지 정보.
     * @return 포스트 정보 목록.
     */
    fun getNewsfeed(page: Page): PaginatedList<Post>

}