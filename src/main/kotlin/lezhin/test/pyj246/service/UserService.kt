package lezhin.test.pyj246.service

import lezhin.test.pyj246.persistence.tables.records.UserRecord

/**
 * 사용자 서비스 인터페이스.
 */
interface UserService {

    /**
     * 사용자 레코드 조회.
     * @param userId    사용자 아이디.
     * @return 사용자 레코드.
     */
    fun getUserRecordByUserId(userId: String): UserRecord

    /**
     * 사용자 레코드 조회.
     * @param email     이메일.
     * @return 사용자 레코드.
     */
    fun getUserRecordByEmail(email: String): UserRecord
}