package lezhin.test.pyj246.controller

import io.swagger.annotations.*
import lezhin.test.pyj246.model.page.Page
import lezhin.test.pyj246.model.page.PaginatedList
import lezhin.test.pyj246.model.post.Post
import lezhin.test.pyj246.service.PostService
import lezhin.test.pyj246.service.UserService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * 뉴스피드 컨트롤러.
 */
@RequestMapping
@RestController
class NewsfeedController(
    private val postService: PostService,
    private val userService: UserService
) {

    /**
     * 사용자 뉴스피드 조회.
     */
    @ApiOperation(value = "사용자 뉴스피드 조회", notes = "로그인한 사용자의 팔로잉중인 사용자의 포스트 목록을 조회한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "no", value = "페이지 번호",
            required = false, paramType = "query", dataType = "int", example = "1"),
        ApiImplicitParam(name = "limit", value = "페이지 당 노출 데이터 수",
            required = false, paramType = "query", dataType = "int", example = "10")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @GetMapping("/v1/newsfeed")
    fun getNewsfeed(
        @AuthenticationPrincipal jwt: Jwt,
        @RequestParam("no", required = false) no: Int?,
        @RequestParam("limit", required = false) limit: Int?
    ): PaginatedList<Post> {
        val user = userService.getUserRecordByEmail(jwt.subject)
        return postService.getNewsfeed(user, Page(no, limit))
    }

    /**
     * 모든 포스트 조회.
     */
    @ApiOperation(value = "모든 포스트 조회", notes = "모든 사용자의 포스트 목록을 조회한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "no", value = "페이지 번호",
            required = false, paramType = "query", dataType = "int", example = "1"),
        ApiImplicitParam(name = "limit", value = "페이지 당 노출 데이터 수",
            required = false, paramType = "query", dataType = "int", example = "10")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @GetMapping("/v1/newsfeed/all")
    fun getAllNewsfeed(
        @RequestParam("no", required = false) no: Int?,
        @RequestParam("limit", required = false) limit: Int?
    ): PaginatedList<Post> {
        return postService.getNewsfeed(Page(no, limit))
    }
}