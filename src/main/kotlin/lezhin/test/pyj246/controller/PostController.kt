package lezhin.test.pyj246.controller

import io.swagger.annotations.*
import lezhin.test.pyj246.model.page.Page
import lezhin.test.pyj246.model.page.PaginatedList
import lezhin.test.pyj246.model.post.Post
import lezhin.test.pyj246.model.post.PostRequest
import lezhin.test.pyj246.service.PostService
import lezhin.test.pyj246.service.UserService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.*

/**
 * 포스트 컨트롤러.
 */
@RequestMapping("/v1/posts")
@RestController
class PostController(
    private val postService: PostService,
    private val userService: UserService
) {
    /**
     * 포스트 생성.
     */
    @ApiOperation(value = "포스트 생성", notes = "포스트를 생성한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @PostMapping
    fun createPost(
        @AuthenticationPrincipal jwt: Jwt,
        @RequestBody post: PostRequest
    ) {
        val user = userService.getUserRecordByEmail(jwt.subject)
        postService.createPost(post, user)
    }

    /**
     * 포스트 수정.
     */
    @ApiOperation(value = "포스트 수정", notes = "포스트를 수정한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "postKey", value = "포스트 식별자",
            required = true, paramType = "path", dataType = "long", example = "1")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @PutMapping("/{postKey}")
    fun updatePost(
        @AuthenticationPrincipal jwt: Jwt,
        @PathVariable("postKey") postKey: Long,
        @RequestBody post: PostRequest
    ) {
        val user = userService.getUserRecordByEmail(jwt.subject)
        postService.updatePost(postKey, post, user)
    }

    /**
     * 포스트 삭제.
     */
    @ApiOperation(value = "포스트 삭제", notes = "포스트를 삭제한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "postKey", value = "포스트 식별자",
            required = true, paramType = "path", dataType = "long", example = "1")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @DeleteMapping("/{postKey}")
    fun deletePost(
        @AuthenticationPrincipal jwt: Jwt,
        @PathVariable("postKey") postKey: Long
    ) {
        val user = userService.getUserRecordByEmail(jwt.subject)
        postService.deletePost(postKey, user)
    }

    /**
     * 포스트 조회.
     */
    @ApiOperation(value = "포스트 조회", notes = "포스트를 조회한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "postKey", value = "포스트 식별자",
            required = true, paramType = "path", dataType = "long", example = "1")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @GetMapping("/{postKey}")
    fun getPost(
        @PathVariable("postKey") postKey: Long
    ): Post {
        return postService.getPost(postKey)
    }

    /**
     * 사용자 포스트 목록 조회.
     */
    @ApiOperation(value = "사용자 포스트 목록 조회", notes = "로그인한 사용자의 포스트 목록을 조회한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "no", value = "페이지 번호",
            required = false, paramType = "query", dataType = "int", example = "1"),
        ApiImplicitParam(name = "limit", value = "페이지 당 노출 데이터 수",
            required = false, paramType = "query", dataType = "int", example = "10")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @GetMapping
    fun getPostList(
        @AuthenticationPrincipal jwt: Jwt,
        @RequestParam("no", required = false) no: Int?,
        @RequestParam("limit", required = false) limit: Int?
    ): PaginatedList<Post> {
        val user = userService.getUserRecordByEmail(jwt.subject)
        return postService.getPostList(user, Page(no, limit))
    }
}