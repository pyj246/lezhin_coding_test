package lezhin.test.pyj246.controller

import io.swagger.annotations.*
import lezhin.test.pyj246.model.user.User
import lezhin.test.pyj246.service.FollowService
import lezhin.test.pyj246.service.UserService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.*

/**
 * 팔로우 컨트롤러.
 */
@RequestMapping("/v1/following")
@RestController
class FollowController(
    private val followService: FollowService,
    private val userService: UserService
) {

    /**
     * 팔로우한 사용자 목록 조회.
     */
    @ApiOperation(value = "팔로우한 사용자 목록 조회", notes = "로그인한 사용자가 팔로잉중인 사용자 목록을 조회한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @GetMapping("")
    fun getFollowingList(
        @AuthenticationPrincipal jwt: Jwt
    ): List<User> {
        val user = userService.getUserRecordByEmail(jwt.subject)
        return followService.getFollowingList(user)
    }

    /**
     * 특정 사용자를 팔로우하는 사용자 목록.
     */
    @ApiOperation(value = "특정 사용자를 팔로우하는 사용자 목록", notes = "특정 사용자를 팔로우하는 사용자 목록을 조회한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "userId", value = "사용자 아이디",
            required = true, paramType = "path", dataType = "string", example = "USER-ID-01")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @GetMapping("/{userId}")
    fun getFollowerList(
        @PathVariable("userId") userId: String
    ): List<User> {
        val user = userService.getUserRecordByUserId(userId)
        return followService.getFollowerList(user)
    }

    /**
     * 팔로우 생성.
     */
    @ApiOperation(value = "팔로우 생성", notes = "특정 사용자를 팔로우한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "userId", value = "사용자 아이디",
            required = true, paramType = "path", dataType = "string", example = "USER-ID-01")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @PostMapping("/{userId}")
    fun createFollowing(
        @AuthenticationPrincipal jwt: Jwt,
        @PathVariable("userId") userId: String
    ) {
        val user = userService.getUserRecordByEmail(jwt.subject)
        val following = userService.getUserRecordByUserId(userId)
        followService.createFollowing(user, following)
    }

    /**
     * 팔로우 삭제.
     */
    @ApiOperation(value = "팔로우 삭제", notes = "특정 사용자의 팔로잉을 취소한다.")
    @ApiImplicitParams(
        ApiImplicitParam(name = "Authorization", value = "Bearer access token",
            required = true, paramType = "header", dataType = "string"),
        ApiImplicitParam(name = "userId", value = "사용자 아이디",
            required = true, paramType = "path", dataType = "string", example = "USER-ID-01")
    )
    @ApiResponses(
        ApiResponse(code = 200, message = "처리 성공"),
        ApiResponse(code = 400, message = "잘못된 접근"),
        ApiResponse(code = 500, message = "서버에러")
    )
    @DeleteMapping("/{userId}")
    fun deleteFollowing(
        @AuthenticationPrincipal jwt: Jwt,
        @PathVariable("userId") userId: String
    ) {
        val user = userService.getUserRecordByEmail(jwt.subject)
        val following = userService.getUserRecordByUserId(userId)
        followService.deleteFollowing(user, following)
    }

}