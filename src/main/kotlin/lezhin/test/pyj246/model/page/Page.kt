package lezhin.test.pyj246.model.page

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

@ApiModel(description = "페이징 처리 모델")
data class PaginatedList<T>(

    @ApiModelProperty("페이지 모델")
    val page: Page,

    @ApiModelProperty("결과 목록")
    val list: List<T> = mutableListOf()
)

@ApiModel(description = "페이징 모델")
data class Page(

    @ApiModelProperty("페이지 번호")
    val no: Int,

    @ApiModelProperty("페이지 당 노출 데이터 수")
    val limit: Int,

    @ApiModelProperty("전체 항목 수")
    var total: Int? = null
) {
    constructor(no: Int?, limit: Int?) : this(no ?: 1, limit ?: 20, null)
}