package lezhin.test.pyj246.model.user

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

@ApiModel(description = "사용자 정보 모델")
data class User (

    @ApiModelProperty("사용자 아이디")
    val userId: String,

    @ApiModelProperty("사용자 별칭")
    val alias: String
)