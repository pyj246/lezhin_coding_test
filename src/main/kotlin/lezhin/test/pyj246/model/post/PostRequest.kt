package lezhin.test.pyj246.model.post

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.time.OffsetDateTime

@ApiModel(description = "포스트 정보 요청 모델")
data class PostRequest (

    @ApiModelProperty(value = "컨텐츠")
    val contents: String
)