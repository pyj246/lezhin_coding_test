package lezhin.test.pyj246.model.post

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.time.OffsetDateTime

@ApiModel(description = "포스트 정보 모델")
data class Post (

    @ApiModelProperty("포스트 식별자")
    val postKey: Long? = null,

    @ApiModelProperty("컨텐츠")
    val contents: String,

    @ApiModelProperty("작성자 별칭")
    val writerAlias: String?,

    @ApiModelProperty("작성자 아이디")
    val writerId: String?,

    @ApiModelProperty("생성일시")
    val createAt: OffsetDateTime? = null,

    @ApiModelProperty("수정일시")
    val updateAt: OffsetDateTime? = null
)